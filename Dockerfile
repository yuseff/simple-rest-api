# Define base image
FROM microsoft/dotnet:2.1-sdk AS build-env

# Copy project files
WORKDIR /source
COPY ["src/SimpleAPI/SimpleAPI.csproj", "./SimpleAPI/SimpleAPI.csproj"]

# Restore
RUN dotnet restore "./SimpleAPI/SimpleAPI.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR /source/src/SimpleAPI
RUN dotnet publish -c Release -o /publish

# Runtime
FROM microsoft/dotnet:2.1-aspnetcore-runtime
EXPOSE 5000
EXPOSE 5001
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "SimpleAPI.dll"]